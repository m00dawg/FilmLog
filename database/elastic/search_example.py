"""
GET filmlog/_search/
  {
  "query": {
    "bool": {
      "must": [
        { "match": { "binders.user_id": 1        }}
      ],
      "filter": [
      {
        "multi_match" : {
          "query":    "What"
        }
      }
      ]
    }
  }
}

GET filmlog_films/_search/
  {
  "query": {
    "bool": {
      "must": [
        { "match": { "user_id": 1        }}
      ],
      "filter": [
      {
        "multi_match" : {
          "query":    "school"
        }
      }
      ]
    }
  }
}

"""

import os
import configparser
from datetime import datetime
from elasticsearch import Elasticsearch

config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '../../config.ini'))

elasticsearch = Elasticsearch(config.get('elasticsearch', 'url'))

search_string = {
  "query": {
    "bool": {
      "must": [
        { "match": { "user_id": 1        }}
      ],
      "filter": [
      {
        "multi_match" : {
          "query":    "format"
        }
      }
      ]
    }
  }
}

result = elasticsearch.search(body=search_string,index='filmlog_*')
print(result)
