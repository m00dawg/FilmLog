import os
import configparser
from datetime import datetime
from elasticsearch import Elasticsearch
from sqlalchemy import create_engine
from sqlalchemy.sql import text



config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '../../config.ini'))

elasticsearch = Elasticsearch(config.get('elasticsearch', 'url'))

db_url = os.getenv('DB_URL')
if not db_url:
    db_url = config.get('database', 'url')



engine = create_engine(db_url,
                       pool_recycle=config.getint('database', 'pool_recycle'),
                       pool_size=config.getint('database', 'pool_size'))
connection = engine.connect()


# Purge Elastic Search
elasticsearch.delete_by_query(index='filmlog_binders', body='{"query": {"match_all":{}}}')
elasticsearch.delete_by_query(index='filmlog_projects', body='{"query": {"match_all":{}}}')
elasticsearch.delete_by_query(index='filmlog_films', body='{"query": {"match_all":{}}}')

# Now fully populate all the indexes from scratch

# Binders
qry = text("""SELECT userID, binderID, name, createdOn, notes FROM Binders""")
binders_query = connection.execute(qry).fetchall()
for row in binders_query:
    binder = {
        "user_id" : row['userID'],
        "binder_id" : row['binderID'],
        "name" : row['name'],
        "created_on" : row['createdOn'],
        "notes" : row['notes']
    }
    binder_id = str(row['userID']) + ':' + str(row['binderID'])
    elasticsearch.create(index='filmlog_binders', id=binder_id, document=binder)

# Projects
qry = text("""SELECT userID, binderID, projectID, name, createdOn, notes
    FROM Projects""")
projects_query = connection.execute(qry).fetchall()
for row in projects_query:
    project = {
        "user_id" : row['userID'],
        "binder_id" : row['binderID'],
        "project_id" : row['projectID'],
        "name" : row['name'],
        "created_on" : row['createdOn'],
        "notes" : row['notes']
    }
    project_id = str(row['userID']) + ':' + str(row['projectID'])
    elasticsearch.create(index='filmlog_projects', id=project_id, document=project)

# Films
qry = text("""SELECT Films.userID, projectID, filmID, Cameras.cameraID,
    Cameras.name AS cameraName, Lenses.lensID, Lenses.name AS lensName,
    FilmTypes.filmTypeID, FilmTypes.name AS filmTypeName,
    FilmTypes.iso AS filmTypeISO, FilmSizes.filmSizeID,
    FilmSizes.size AS filmSizeSize, FilmSizes.type AS filmSizeType,
    FilmSizes.format AS filmSizeFormat,
    Films.iso, fileDate, loaded, unloaded,
    developed, fileNo, title, development, Films.notes
    FROM Films
    LEFT JOIN Cameras ON Cameras.cameraID = Films.cameraID
        AND Cameras.userID = Films.userID
    LEFT JOIN Lenses ON Lenses.lensID = Films.lensID
        AND Lenses.userID = Films.userID
    LEFT JOIN FilmTypes ON FilmTypes.filmTypeID = Films.filmTypeID
        AND FilmTypes.userID = Films.userID
    LEFT JOIN FilmSizes ON FilmSizes.filmSizeID = Films.filmSizeID""")
films_query = connection.execute(qry).fetchall()
for row in films_query:
    film = {
        "user_id" : row['userID'],
        "project_id" : row['projectID'],
        "film_id" : row['filmID'],
        "camera.id" : row['cameraID'],
        "camera.name" : row['cameraName'],
        "lens.id" : row['lensID'],
        "lens.name" : row['lensName'],
        "film_type.id" : row['filmTypeID'],
        "film_type.name" : row['filmTypeName'],
        "film_type.iso" : row['filmTypeISO'],
        "film_size.id" : row['filmSizeID'],
        "film_size.size" : row['filmSizeSize'],
        "film_size.type" : row['filmSizeType'],
        "film_size.format" : row['filmSizeFormat'],
        "iso" : row['iso'],
        "fileDate" : row['fileDate'],
        "loaded" : row['loaded'],
        "unloaded" : row['unloaded'],
        "developed" : row['developed'],
        "file_no" : row['fileNo'],
        "title" : row['title'],
        "development" : row['development'],
        "notes" : row['notes']
    }
    film_id = str(row['userID']) + ':' + str(row['filmID'])
    elasticsearch.create(index='filmlog_films', id=film_id, document=film)
